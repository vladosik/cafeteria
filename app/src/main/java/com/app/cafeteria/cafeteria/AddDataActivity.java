package com.app.cafeteria.cafeteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import static com.app.cafeteria.cafeteria.DBHelper.addData;
import static com.app.cafeteria.cafeteria.DBHelper.removeDataById;

public class AddDataActivity extends AppCompatActivity {

    FeedReaderDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);

        mDbHelper = new FeedReaderDbHelper(this);

        final EditText description_txt = findViewById(R.id.description_txt);
        final EditText img_txt = findViewById(R.id.editText);

        Button add_btn = findViewById(R.id.add_btn);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!description_txt.getText().toString().isEmpty() && !img_txt.getText().toString().isEmpty()) {

                    removeDataById(mDbHelper, "favourite");
                    addData(description_txt.getText().toString(), img_txt.getText().toString(), mDbHelper);
                    //writeFile(description_txt.getText().toString(), img_txt.getText().toString(), AddDataActivity.this);

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }
            }
        });
    }
}
