package com.app.cafeteria.cafeteria;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.FacebookSdk;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.cafeteria.cafeteria.DBHelper.getDescriptionById;
import static com.app.cafeteria.cafeteria.DBHelper.insertAllData;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FeedReaderDbHelper mDbHelper;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        mDbHelper = new FeedReaderDbHelper(this);

        if(getDescriptionById(mDbHelper,"makiyato", this).isEmpty()) {
            insertAllData(mDbHelper);
        }

        Fragment fragment = DetailsFragment.newInstance(R.id.makiyato, mDbHelper, this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;

        int id = item.getItemId();

        if (id == R.id.makiyato) {
            fragment = DetailsFragment.newInstance(1, mDbHelper, this);
        } else if (id == R.id.americano) {
            fragment = DetailsFragment.newInstance(2, mDbHelper, this);
        } else if (id == R.id.cappuccino) {
            fragment = DetailsFragment.newInstance(3, mDbHelper, this);
        } else if (id == R.id.latte) {
            fragment = DetailsFragment.newInstance(4, mDbHelper, this);
        } else if (id == R.id.mokkochino) {
            fragment = DetailsFragment.newInstance(5, mDbHelper, this);
        } else if (id == R.id.favourite) {
            fragment = DetailsFragment.newInstance(6, mDbHelper, this);
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
}
