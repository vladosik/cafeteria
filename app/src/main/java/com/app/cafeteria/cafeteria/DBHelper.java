package com.app.cafeteria.cafeteria;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.content.Context.MODE_PRIVATE;

public class DBHelper {


    static String getDescriptionById(FeedReaderDbHelper mDbHelper, String element, Context context){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME,
                FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION,
                FeedReaderContract.FeedEntry.COLUMN_IMAGE
        };

        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME + " = ?";
        String[] selectionArgs = { element };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                FeedReaderContract.FeedEntry.COLUMN_NAME + " DESC";

        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        String itemId = "";
        while(cursor.moveToNext()) {
            itemId = cursor.getString(
                    cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION));
        }

        cursor.close();

        return readFile(itemId, context);
    }


    static public String getImageById(FeedReaderDbHelper mDbHelper, String element, Context context){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME,
                FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION,
                FeedReaderContract.FeedEntry.COLUMN_IMAGE
        };

        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME + " = ?";
        String[] selectionArgs = { element };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                FeedReaderContract.FeedEntry.COLUMN_NAME + " DESC";

        Cursor cursor = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        String itemId = "";
        while(cursor.moveToNext()) {
            itemId = cursor.getString(
                    cursor.getColumnIndex(FeedReaderContract.FeedEntry.COLUMN_IMAGE));
        }

        cursor.close();

        return readFile(itemId, context);
    }


    static void removeDataById(FeedReaderDbHelper mDbHelper, String element){

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define 'where' part of query.
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME + " LIKE ?";

        // Specify arguments in placeholder order.

        String[] selectionArgs = { element };
        // Issue SQL statement.

        db.delete(FeedReaderContract.FeedEntry.TABLE_NAME, selection, selectionArgs);
    }

    static void addData(String description_txt, String img_txt,FeedReaderDbHelper mDbHelper){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues favourite = new ContentValues();
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "favourite");
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, description_txt);
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, img_txt);

        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, favourite);
    }


    static void insertAllData(FeedReaderDbHelper mDbHelper){

        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues makiyato = new ContentValues();
        makiyato.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "makiyato");
        makiyato.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "makiyato_txt");
        makiyato.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Create a new map of values, where column names are the keys
        ContentValues americano = new ContentValues();
        americano.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "americano");
        americano.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "americano_txt");
        americano.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Create a new map of values, where column names are the keys
        ContentValues cappuccino = new ContentValues();
        cappuccino.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "cappuccino");
        cappuccino.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "cappuccino_txt");
        cappuccino.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Create a new map of values, where column names are the keys
        ContentValues latte = new ContentValues();
        latte.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "latte");
        latte.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "latte_txt");
        latte.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Create a new map of values, where column names are the keys
        ContentValues mokkochino = new ContentValues();
        mokkochino.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "mokkochino");
        mokkochino.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "mokkochino_txt");
        mokkochino.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Create a new map of values, where column names are the keys
        ContentValues favourite = new ContentValues();
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_NAME, "favourite");
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_DESCRIPTION, "favourite_txt");
        favourite.put(FeedReaderContract.FeedEntry.COLUMN_IMAGE, "img_base64");

        // Insert the new row, returning the primary key value of the new row
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, makiyato);
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, americano);
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, cappuccino);
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, latte);
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, mokkochino);
        db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, favourite);

    }


    private static String readFile(String file_name, Context context) {
        String finall_text = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(file_name)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                finall_text += mLine;
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        return finall_text;
    }

    static void writeFile(String description_txt, String img_txt, Context context) {

        try {
            // отрываем поток для записи
            BufferedWriter description_bw = new BufferedWriter(new OutputStreamWriter(
                    context.openFileOutput("favourite_txt", MODE_PRIVATE)));
            // пишем данные
            description_bw.write(description_txt);
            // закрываем поток
            description_bw.close();

            // отрываем поток для записи
            BufferedWriter img_bw = new BufferedWriter(new OutputStreamWriter(
                    context.openFileOutput("img_base64", MODE_PRIVATE)));
            // пишем данные
            img_bw.write(img_txt);
            // закрываем поток
            img_bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
