package com.app.cafeteria.cafeteria;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import static com.app.cafeteria.cafeteria.DBHelper.getDescriptionById;
import static com.app.cafeteria.cafeteria.DBHelper.removeDataById;


public class DetailsFragment extends Fragment {

    static int view_id = 0;
    static FeedReaderDbHelper mDbHelpers;
    static Activity contexts;
    TextView description;
    public static DetailsFragment newInstance(int index, FeedReaderDbHelper mDbHelper, Activity context) {

        view_id = index;
        mDbHelpers = mDbHelper;
        contexts = context;
        return new DetailsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ScrollView scroller = new ScrollView(getActivity());
        description = getActivity().findViewById(R.id.description);

        if (savedInstanceState != null) {
            description.setText(savedInstanceState.getCharSequence("savedInstance"));
        }

        Button add_button = getActivity().findViewById(R.id.add_button);
        add_button.setVisibility(View.INVISIBLE);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddDataActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        Button remove_button = getActivity().findViewById(R.id.remove_button);
        remove_button.setVisibility(View.INVISIBLE);
        remove_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                removeDataById(mDbHelpers, "favourite");
                getActivity().finish();
                startActivity(getActivity().getIntent());
            }
        });


        Button share_button = getActivity().findViewById(R.id.share_button);
        share_button.setVisibility(View.INVISIBLE);
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initShareIntent();
            }
        });

        if (container == null) {
            return null;
        }

        switch(view_id) {
            case 1:
                description.setText(getDescriptionById(mDbHelpers,"makiyato", getActivity()));
                break;
            case 2:
                description.setText(getDescriptionById(mDbHelpers,"americano", getActivity()));
                break;
            case 3:
                description.setText(getDescriptionById(mDbHelpers,"cappuccino", getActivity()));
                break;
            case 4:
                description.setText(getDescriptionById(mDbHelpers,"latte", getActivity()));
                break;
            case 5:
                description.setText(getDescriptionById(mDbHelpers,"mokkochino", getActivity()));
                break;
            case 6:
                add_button.setVisibility(View.VISIBLE);
                remove_button.setVisibility(View.VISIBLE);
                share_button.setVisibility(View.VISIBLE);

                description.setText(getDescriptionById(mDbHelpers,"favourite", getActivity()));
                break;
            default:

                break;
        }

        return scroller;
    }

    private void initShareIntent(){

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .setQuote(getDescriptionById(mDbHelpers,"favourite", getActivity()))
                .build();
        ShareDialog.show(contexts, content);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putCharSequence("savedInstance", description.getText());
    }

}